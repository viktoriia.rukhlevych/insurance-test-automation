/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
}

const cucumber = require('cypress-cucumber-preprocessor').default
const browserify = require('@cypress/browserify-preprocessor');
const fs = require('fs')

module.exports = (on, config) => {
  /**
   * Get OS or provided Environment Value, 
   * for select Test Environment. 
   * If ENV not provided, default value 'qa'
   */
  config.env.ENV = process.env.ENV ? (process.env.ENV).toLowerCase() : 'qa';
  console.log(`Environment is: ${config.env.ENV}`);

  /**
   * Change BaseURL base on provided test Environment. 
   */
  config.baseUrl = config.env[config.env.ENV];

  if (!process.env.local && !config.env.TAGS) {
    config.env.TAGS = "@smoke"
  }

  console.log(`TAGS is ${config.env.TAGS}`);

  const options = {
    ...browserify.defaultOptions,
    browserifyOptions: {
      ...browserify.defaultOptions.browserifyOptions,
      paths: [
        "./cypress/support/pageObjects",
        "./cypress/support/helpers",
        "./cypress/support/enums"
      ],
    }
  };
  on('file:preprocessor', cucumber(options));

  /**
   * Gettting Current Browser Info and Store in JSON file, 
   * then this data use for Generation Report
   */
  on('before:browser:launch', (browser = {}, launchOptions) => {
    const name = browser.displayName;
    const version = browser.version;
    const tag = config.env.TAGS;
    const env = config.env.ENV;
    const info = {
      name,
      version,
      tag,
      env
    }
    console.log(info);
    fs.writeFileSync('./browserInfo.json', JSON.stringify(info))
  })

  return config;
}
