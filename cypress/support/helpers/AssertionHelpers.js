class AssertionHelpers {
    static expectVisible(element) {
        element.should('be.visible', `The expected element ${element.title} was not visible on the page`);
    }

    static expectNotPresent(element) {
        element.should('not.exist', `The expected element ${element.title} was visible on the page`);
    }

    static expectToContain(element, expectedText) {
        element.should('contain', expectedText, `the expected element did not contain the expected text: ${expectedText}`);
    }

    static expectElementNotContainText(element, text) {
        element.should('not.contain', text, `the expected element contained the unexpected text: ${text}`);
    }

    static expectUnwrappedElementToContain(elementUnwrapped, expectedText) {
        expect(elementUnwrapped.text()).to.contains(expectedText);
    }

    static expectTitleContains(titleText) {
        cy.title().should('contains', titleText, `expected title text: ${titleText} was not found in the title: ${cy.title()}`);
    }

    static expectPageMatch(pageStatus) {
        pageStatus.then((onPageBoolean) => {
            expect(onPageBoolean, "The test was not on the expected page").to.be.true;
        })
    }

    static expectIconElement(element) {
        element.get('i').should('not.eq', undefined, `The element ${element.title}, did not have an icon element among its children`);
    }

    static elementShouldContains(element, expectedText) {
        element.should('contain', expectedText, `element is not contains expected Text ${expectedText}`);
    }

    static elementTextEqual(element, expectedText) {
        element.then($text => {
            expect($text.text()).to.equal(expectedText, `Element does not have expected text: ${expectedText}`)
        });
    }

    static expectNumberIsGreaterThanAnotherNumber(valueToCheck, valueToBeAbove) {
        const firstNumber = Number(valueToCheck);
        const secondNumber = Number(valueToBeAbove);
        assert.isAbove(firstNumber, secondNumber)
    }

    static expectNumberIsLessThanAnotherNumber(valueToCheck, valueToBeBelow) {
        const firstNumber = Number(valueToCheck);
        const secondNumber = Number(valueToBeBelow);
        assert.isAtMost(firstNumber, secondNumber);
    }

    static expectDateInRange(actualdate, startDate, endDate) {
        cy.expect(actualdate >= startDate && actualdate <= endDate, `Actual date: ${actualdate}, date range: ${startDate} -- ${endDate}`).to.be.true;
    }

    static expectArrayEqualsArray(expectedArr, actualArr) {
        expect(expectedArr).to.deep.equal(actualArr, `Arrays are not equal, Expected: ${expectedArr}, Actual: ${actualArr}`);
    }

    static expectElementToContainsAttributeValue(element, attribute, value) {
        element.should('have.attr', attribute).and('contain', value);
    }

    static expectTextEqual(actualText, expectedText) {
        expect(actualText).to.equal(expectedText, `Text is not correct, Actual ${actualText}, expected: ${expectedText}`);
    }

    static expectElementArrayLength(element, expectedLength) {
        element.should('have.length', expectedLength);
    }
}
export default AssertionHelpers