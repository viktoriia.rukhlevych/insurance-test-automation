// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// import 'cypress-file-upload';  --> for file upload
const envVars = Cypress.env();
import InsuranceLandingPage from 'InsuranceLandingPage';
import AssertionHelpers from 'AssertionHelpers';

const insuranceLandingPage = new InsuranceLandingPage();


Cypress.Commands.add("checkPriviliges", (productType, user) => {
    cy.get('@currentUser').then(user => {
        user.priviliges.forEach(privilige => {
            let keyName = Object.keys(privilige)[0];
            insuranceLandingPage.chooseMainTab(keyName);
            let elements = Object.keys(privilige[keyName]);
            if (elements.includes('buttons')) {
                let expectedButtons = privilige[keyName].buttons;
                expectedButtons.forEach(button => {
                    AssertionHelpers.expectVisible(insuranceLandingPage.pageButtons(button))
                    // insuranceLandingPage.pageButtons(button).should('be.visible');
                });
            }
            if (elements.includes('tabs')) {
                let expectedTabs = privilige[keyName].tabs;
                expectedTabs.forEach(tab => {
                    insuranceLandingPage.pageTabs(tab).click();
                });
            }
        });
    })
});

Cypress.Commands.add("checkInaccessibility", (productType, user) => {
    cy.get('@currentUser').then(user => {
        user.non_priviliges.forEach(access => {
            AssertionHelpers.expectNotPresent(insuranceLandingPage.mainTab(access));
        })
    })
});

Cypress.Commands.add("waitForAPICall", (methodType, url, expectedStatusCode) => {
    cy.intercept(methodType, `**${url}*`).as('responseAlies');
    cy.wait('@responseAlies').its('response.statusCode').should('eq', expectedStatusCode);
})