class InsuranceLandingPage {
    email() {
        return cy.get(`input[name='username']`);
    }
    password() {
        return cy.get(`input[name='password']`);
    }
    logInButton() {
        return cy.get('.v-btn__content').contains('Log In');
    }
    mainTab(tabName) {
        return cy.get(`.src-lib-components-sidebar-display-19_I`).contains(tabName);
    }
    login(email, password) {
        this.email().type(email);
        this.password().type(password);
        this.logInButton().click();
    }
    chooseMainTab(tabName) {
        this.mainTab(tabName).click();
    }

    sidebarPanel() {
        return cy.get('div [class*="sidebar"]');
    }

    sideBarTabs() {
        return this.sidebarPanel().get('a');
    }

    pageButtons(buttonName) {
        return cy.xpath(`//span[@class='v-btn__content' and contains(text(),'${buttonName}')]`);
    }

    pageTabs(tabName) {
        return cy.xpath(`//*[@role='tab' and contains(.,'${tabName}')]`);
    }

    overviewTabsList() {
        return cy.get(`.v-list-item__content`);
    }

    overviewTabs(tabName) {
        return cy.xpath(`//div[@class='body-2 col col-12 ScoringCategory_label_1fBAv' and contains(text(),'${tabName}')]`);
    }

    overviewPageName() {
        return cy.get(`.subtitle-1.emphasis-med`);
    }

    locationPageName() {
        return cy.get(`.v-toolbar__title.fw-subhead`);
    }

}
export default InsuranceLandingPage;