// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';
require('cypress-xpath');

// Alternatively you can use CommonJS syntax:
// require('./commands')


String.prototype.toPascalCase = function () {
    return this.split(' ')
        .map(word => word.toLowerCase())
        .map(word => word.replace(word.charAt(0), word.charAt(0).toUpperCase()))
        .join('');
}
String.prototype.toCamelCase = function () {
    return this.toPascalCase()
        .replace(this.charAt(0), this.charAt(0).toLowerCase());
}

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
}) 
