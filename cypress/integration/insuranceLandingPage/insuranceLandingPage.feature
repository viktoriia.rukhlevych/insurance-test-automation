@smoke
Feature: Insurance Landing page tests

    Background: Navigating to the QOMPLX landing page
        Given user is on QOMPLX landing page

    Scenario Outline: Access control test
        When user logged in as "<productType>" "<userType>"
        Then I validate that title of the page contains "<title>"
        # And user clicks on "Insurance" tab
        Then user verifies user priviligies for "<productType>" "<userType>"
        Examples:
            | productType  | userType    | title          |
            | Wonder Cover | broker      | Live Quote     |
            | Wonder Cover | underwriter | Overview       |
            | Rubiqon      | broker      | My Submissions |
            | Rubiqon      | underwriter | Ratings Tool   |


    Scenario Outline: Login in as an underwriter and check all subtabs are present on Modeling Review page
        When user logged in as "<productType>" "<userType>"
        # And user clicks on "Insurance" tab
        Then user clicks on "Modeling Review" tab
        #  And user clicks on "Overview" subtab
        Then user verifies all Overview page tabs are functional
        Examples:
            | productType  | userType    |
            | Wonder Cover | underwriter |
            | Rubiqon      | underwriter |

    Scenario Outline: Login as User and verify inaccessibility according to the account privileges
        When user logged in as "<productType>" "<userType>"
        # And user clicks on "Insurance" tab
        Then user verifies account inaccessibility for "<productType>" "<userType>"
        Examples:
            | productType  | userType    |
            | Wonder Cover | broker      |
            | Wonder Cover | underwriter |
            | Rubiqon      | broker      |
            | Rubiqon      | underwriter |




