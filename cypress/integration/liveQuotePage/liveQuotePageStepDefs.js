import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

import AssertionHelpers from 'AssertionHelpers';

Then('user clicks on {string} dropdown', (dropdownName) => {
  cy.get('.v-select__selections').first().click();
});


Then('user verifies all dropdown options are present', () => {
  AssertionHelpers.expectElementArrayLength(cy.get('.v-list-item__content'), 6);
});

Then('user chooses {string} as a search option', (option) => {
  cy.get('.v-list-item__content').contains(option).click();
});
