@smoke
Feature: Live Quote page tests

    Background: Navigating to the QOMPLX landing page
        Given user is on QOMPLX landing page

    Scenario: Verify Searching functionality
        When user logged in as "Wonder Cover" "broker"
        Then user clicks on "Search By:" dropdown
        Then user verifies all dropdown options are present
        Then user chooses "Quote ID" as a search option
