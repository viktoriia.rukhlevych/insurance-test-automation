import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';
import InsuranceLandingPage from 'InsuranceLandingPage';
import AssertionHelpers from 'AssertionHelpers';

const insuranceLandingPage = new InsuranceLandingPage();

Given('user is on QOMPLX landing page', () => {
    // cy.task('log', Cypress.env());
    cy.visit("/");

});

When('user logged in as {string} {string}', (productType, user) => {
    cy.fixture(`users/${Cypress.env('ENV')}/${productType}/${user}.json`).as('currentUser');
    cy.get('@currentUser').then(user => {
        insuranceLandingPage.login(user.email, user.password);
    })
});

Then("I validate that title of the page contains {string}", (title) => {
    AssertionHelpers.expectTitleContains(title);
});

Then('user verifies user priviligies for {string} {string}', (productType, user) => {
    cy.checkPriviliges(productType, user);
});

Then('user clicks on {string} tab', (tabName) => {
    cy.waitForAPICall("POST", "http://insurance.qweb.qos/deprecated-api/policy/list", 200);
    insuranceLandingPage.chooseMainTab(tabName);
});

Then('user verifies all Overview page tabs are functional', () => {
    insuranceLandingPage.overviewTabsList()
        .each(function ($el, index, $listOfElements) {
            cy.get($el).click();
            if ($el.text().includes('Location')) {
                insuranceLandingPage.locationPageName()
                    .should('be.visible')
                    .and('contain.text', $el.text().trim().substring(0, 7));
            } else {
                insuranceLandingPage.overviewPageName()
                    .should('be.visible')
                    .and('contain.text', $el.text().trim());
            }
        })
});

Then('user verifies account inaccessibility for {string} {string}', (productType, user) => {
    cy.checkInaccessibility(productType, user);
});

